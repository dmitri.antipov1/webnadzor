import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  contactForm: FormGroup;
  isModal = false;
  isSend = false;

  constructor(private fb: FormBuilder, private http: HttpClient) {
    this.contactForm = this.fb.group({
      userQuestion: ['Регистрация в ЕГИСЗ', Validators.required],
      organizationName: ['', Validators.required],
      userEmail: ['', [Validators.required, Validators.email]],
      userMassage: ['', Validators.required],
      policy: [false, Validators.pattern('true')],
    });
  }

  ngOnInit(): void {
  }

  sendFormData(): void {
    if (this.contactForm.valid) {
      const fd = new FormData()
      fd.append('nameService', this.contactForm.controls['userQuestion'].value);
      fd.append('nameOrganization', this.contactForm.controls['organizationName'].value);
      fd.append('nameMail', this.contactForm.controls['userEmail'].value);
      fd.append('message', this.contactForm.controls['userMassage'].value);
      fd.append('policy', this.contactForm.controls['policy'].value);
      this.http.post(
        'https://forms.xn--80abckcp3bls.xn--p1ai/ingress/form/1c940b992e6604761388e0ffde19ac08',
        fd).subscribe({
        next: () => {
          this.isSend = true;
          this.contactForm.reset();
        },
        error: () => {
          console.log('Error');
        }
      });
    }
  }

  closeModal($event: any) {
    this.isModal = $event;
  }

  openModal() {
    this.isModal = true;
  }
}
