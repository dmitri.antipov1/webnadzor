import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  mobileWidth = window.innerWidth;
  modal1 = false;
  modal2 = false;

  constructor() { }

  ngOnInit(): void {
  }

  closeModal($event: any) {
    this.modal1 = $event;
  }

  closeModal2($event: any) {
    this.modal2 = $event;
  }
}
