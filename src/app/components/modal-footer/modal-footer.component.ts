import {Component, ElementRef, EventEmitter, HostListener, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-modal-footer',
  templateUrl: './modal-footer.component.html',
  styleUrls: ['./modal-footer.component.css']
})
export class ModalFooterComponent implements OnInit {
  @ViewChild('box') public box: ElementRef | undefined;
  @Output() isModalEvent = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  @HostListener('document:click', ['$event'])
  clickOut(event: Event): void {
    if (this.box?.nativeElement.contains(event.target)) {
      event.stopPropagation();
      this.isModalEvent.emit(false);
    }
  }

}
