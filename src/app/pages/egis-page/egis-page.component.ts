import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-egis-page',
  templateUrl: './egis-page.component.html',
  styleUrls: ['./egis-page.component.css']
})
export class EgisPageComponent implements OnInit {
  egisForm: FormGroup;
  price = 0;
  isModal = false;
  isSend = false;

  constructor(private fb: FormBuilder, private http: HttpClient) {
    this.egisForm = this.fb.group({
      organization: ['', Validators.required],
      isRegistration: ['5000', Validators.required],
      personal: ['1500', Validators.required],
      equipment: ['5000', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      policy: [false, Validators.pattern('true')],
    })
  }

  ngOnInit(): void {
  }

  calculate(): void {
    if (this.egisForm.valid) {
      const personalPrice = parseInt(this.egisForm.get('personal')?.value);
      const equipmentPrice = parseInt(this.egisForm.get('equipment')?.value);
      const registrationPrice = parseInt(this.egisForm.get('isRegistration')?.value);
      this.price = personalPrice + equipmentPrice + registrationPrice;
    }
  }

  sendForm(): void {
    if (this.egisForm.valid) {
      const fd = new FormData()
      fd.append('nameOrganization', this.egisForm.controls['organization'].value);
      fd.append('needRegistration', this.egisForm.controls['isRegistration'].value);
      fd.append('amountWorkers', this.egisForm.controls['personal'].value);
      fd.append('amountDevices', this.egisForm.controls['equipment'].value);
      fd.append('nameMail', this.egisForm.controls['email'].value);
      fd.append('phone', this.egisForm.controls['phone'].value);
      fd.append('policy', this.egisForm.controls['policy'].value);
      this.http.post(
        'https://forms.xn--80abckcp3bls.xn--p1ai/ingress/form/07555222f5206ba1fcce68a41fb656e2',
        fd).subscribe({
        next: (res) => {
          this.isSend = true;
        },
        error: () => {
          console.log('Error');
        }
      });
    }
  }

  openModal() {
    this.isModal = true;
  }

  closeModal($event: any) {
    this.isModal = $event;
  }
}
